## Secure Notepad

Django project to edit notepads with a unique link, encrypting and decrypting on the client side.

The concept is quite simple, and only requires some minimal setup. The webserver in production runs on AWS's EC2, with a postgres database on RDS, and static files served through CloudFront from S3.

Users are invited to create a 'notepad', editable with a JavaScript [TinyMCE WYSIWYG][tiny] editor. They can then encrypt their text with a password using [CryptoJS][cyptojs] in the browser. The encrypted string is then posted to Django using AJAX and saved in a model. Django generates a unique link for each notepad using Python's [uuid.uuid4][uuid4] function. Users can then visit their link, enter their password in a popup form, and the text is decrypted in the browser.

Passwords are never sent to the server, and encryption/decryption all happen on the client side.

This project is live at [https://securenotepad.site][securenotepad]

##### Screenshot:

![](images/screenshot.png?raw=true)


##### Installation:

- clone this project
- change into project directory with ```cd src```
- install requirements with ```pip install -r requirements.txt```
- generate your settings file with ```cp src/securenotepad/settings/sample.settings.env src/securenotepad/settings/settings.env``` and add your custom settings
- run your local server with ```python manage.py runserver```
- navigate to ```http://127.0.0.1:8000``` to explore

[tiny]: https://www.tiny.cloud/
[cyptojs]: https://cryptojs.gitbook.io/docs/
[uuid4]: https://docs.python.org/3/library/uuid.html#uuid.uuid4
[securenotepad]: https://securenotepad.site