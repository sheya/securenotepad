import uuid

from django.db import models
from django.urls import reverse
from django.contrib.sites.shortcuts import get_current_site


class Notepad(models.Model):
    slug = models.UUIDField(default=uuid.uuid4, unique=True)
    notepad = models.TextField(blank=True, null=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def full_url(self, request):
        site = get_current_site(request)
        return f'{request.scheme}://{site.domain}{self.absolute_url}'

    @property
    def absolute_url(self):
        return reverse('notepad:update', kwargs={'slug': self.slug})
