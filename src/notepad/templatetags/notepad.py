from django import template


register = template.Library()


@register.simple_tag(takes_context=True)
def full_url(context, notepad):
    '''
    Generate full url for notepad including domain
    '''
    if notepad:
        return notepad.full_url(context['request'])
