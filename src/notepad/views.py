from django.views import generic

from .mixins import NotepadCreateUpdateMixin


class NotepadCreateView(NotepadCreateUpdateMixin, generic.CreateView):
    pass


class NotepadUpdateView(NotepadCreateUpdateMixin, generic.UpdateView):
    pass


class AboutView(generic.TemplateView):
    template_name = 'notepad/about.html'
