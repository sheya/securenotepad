from django.utils import formats
from django.http import JsonResponse

from .models import Notepad
from .forms import NotepadForm


class NotepadCreateUpdateMixin:
    model = Notepad
    form_class = NotepadForm
    template_name = 'notepad/create_update.html'

    def form_valid(self, form):
        valid = super().form_valid(form)
        if self.request.is_ajax:
            return JsonResponse({
                'result': 'saved',
                'url': self.object.full_url(self.request),
                'created_at': formats.date_format(self.object.created_at, 'DATETIME_FORMAT'),
                'updated_at': formats.date_format(self.object.updated_at, 'DATETIME_FORMAT'),
            })
        return valid

    def get_success_url(self):
        return self.object.absolute_url
