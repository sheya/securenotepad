from django.urls import path

from . import views


app_name = 'notepad'
urlpatterns = [
    path('', views.NotepadCreateView.as_view(), name='create'),
    path('<uuid:slug>', views.NotepadUpdateView.as_view(), name='update'),
    path('about', views.AboutView.as_view(), name='about'),
]
