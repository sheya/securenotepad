let passwordForm = '' +
    '<form action="" class="formName" onsubmit="return false;">' +
    '<div class="form-group">' +
    '<label>Enter your password</label>' +
    '<input type="password" placeholder="Your password" class="form-control pp" autocomplete="none" required />' +
    '</div>' +
    '</form>';


$('#id_notepad').change((e) => {
    $('.save-btn').attr('disabled', $('#isnew').val() === '0' && e.target.value.length <= 0);
});


function pageLoaded () {
    $('.loader').slideUp().remove();
    $('.show-on-load').slideDown('fast');
}


function updateInput () {
    $('#id_notepad').val(tinyMCE.activeEditor.getContent()).trigger('change');
}


function errorAlert (message) {
    return $.alert({
        type: 'red',
        title: 'Error',
        escapeKey: 'ok',
        typeAnimated: true,
        columnClass: 'medium',
        content: message || 'An error has occurred',
        buttons: {
            ok: {
                keys: ['enter'],
                action: function () {},
                btnClass: 'btn-secondary',
            }
        }
    })
}


function encrypt (btn) {
    $(btn).blur();

    $.confirm({
        title: 'Encrypt',
        escapeKey: 'cancel',
        content: passwordForm,
        buttons: {
            encrypt: {
                text: 'Encrypt',
                keys: ['enter'],
                btnClass: 'btn-primary',
                action: function() {
                    let self = this;

                    let pp = this.$content.find('.pp').val();
                    if (pp) {
                        updateInput();
                        let inp = $('#id_notepad');
                        let enc = CryptoJS.AES.encrypt(inp.val(), pp).toString();
                        inp.val(enc);

                        let form = $('.notepad-form');
                        self.showLoading();
                        $.post(form.attr('action'), form.serialize()).then(resp => {
                            $('#link').val(resp.url);
                            $('#notepad-link').html(resp.url);
                            $('.link-container').removeClass('d-none');

                            $('.created-at').text(resp.created_at);
                            $('.updated-at').text(resp.updated_at);
                            $('.meta').removeClass('d-none');

                            self.close();
                        }, err => {
                            errorAlert('An error has occurred, please try again.');
                            self.close()
                        });
                        return false;
                    } else {
                        errorAlert('Please provide a password.');
                        return false;
                    }
                }
            },
            cancel: {
                keys: ['escape'],
                btnClass: 'btn-light',
                action: function () {},
            },
        },
    })
}


function decrypt () {
    $.confirm({
        title: 'Decrypt',
        theme: 'supervan',
        content: passwordForm,
        buttons: {
            decrypt: {
                text: 'Decrypt',
                keys: ['enter'],
                btnClass: 'btn-primary',
                action: function() {
                    let pp = this.$content.find('.pp').val();
                    if (pp) {
                        let content = $('#id_notepad').text();
                        let dec = CryptoJS.AES.decrypt(content, pp).toString(CryptoJS.enc.Utf8);
                        tinyMCE.get()[0].setContent(dec);
                    } else {
                        errorAlert('Please provide a password.');
                        return false;
                    }
                }
            },
        },
        onContentReady: function () {
            this.$content.find('input').focus()
        },
    })
}


function copyLink (btn) {
    $(btn).blur();

    let temp = $("<input>");
    $('body').append(temp);
    temp.val($('#notepad-link').text()).select();
    document.execCommand('copy');
    temp.remove();

    let copyEl = $('.copy');
    copyEl.attr('title', 'Copied!');
    copyEl.tooltip('show');

    setTimeout(() => {
        copyEl.tooltip('dispose');
        copyEl.attr('title', 'Copy');
    }, 1000);
}


$(function() {
    tinymce.init({
        menubar: '',
        statusbar: false,
        selector: '#tiny',
        skin: 'oxide-dark',
        content_css: 'dark',
        height: window.innerHeight * .70,
        setup: function(editor) {
            editor.on('init', () => {
                pageLoaded();
            });
            editor.on('input change', () => {
                updateInput();
            });
        }
    });

    if ($('#isnew').val() === '1') {
        decrypt()
    }
});