from .base import *  # NOQA

import logging.config


# For security and performance reasons, DEBUG is turned off
DEBUG = False
TEMPLATE_DEBUG = False


# Strict password authentication and validation
# To use this setting, install the Argon2 password hashing algorithm.
PASSWORD_HASHERS = [
    'django.contrib.auth.hashers.Argon2PasswordHasher',
    'django.contrib.auth.hashers.PBKDF2PasswordHasher',
    'django.contrib.auth.hashers.PBKDF2SHA1PasswordHasher',
    'django.contrib.auth.hashers.BCryptSHA256PasswordHasher',
    'django.contrib.auth.hashers.BCryptPasswordHasher',
]
AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator'
    },
    {'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator'},
    {'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator'},
    {'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator'},
]


# Must mention ALLOWED_HOSTS in production!
ALLOWED_HOSTS = [
    '.securenotepad.site',
]


# Cache the templates in memory for speed-up
loaders = [
    (
        'django.template.loaders.cached.Loader',
        [
            'django.template.loaders.filesystem.Loader',
            'django.template.loaders.app_directories.Loader',
        ],
    )
]


# Define STATIC_ROOT for the collectstatic command
STATIC_ROOT = str(BASE_DIR.parent / 'site' / 'static')


# Log everything to the logs directory at the top
LOGFILE_ROOT = BASE_DIR.parent / 'logs'


# Use AWS S3 for file storage
AWS_DEFAULT_ACL = None
AWS_S3_SIGNATURE_VERSION = 's3v4'
AWS_S3_CUSTOM_DOMAIN = 'cdn.sheyabernstein.com'
AWS_ACCESS_KEY_ID = env('AWS_ACCESS_KEY_ID')
AWS_SECRET_ACCESS_KEY = env('AWS_SECRET_ACCESS_KEY')
AWS_STORAGE_FOLDER_NAME = env('AWS_STORAGE_FOLDER_NAME')
AWS_STORAGE_BUCKET_NAME = env('AWS_STORAGE_BUCKET_NAME')

STATIC_S3_PATH = '{}/static'.format(AWS_STORAGE_FOLDER_NAME)
STATICFILES_STORAGE = 's3_folder_storage.s3.StaticStorage'
STATIC_ROOT = '/{}/'.format(STATIC_S3_PATH)
STATIC_URL = '{}{}'.format(AWS_S3_CUSTOM_DOMAIN, STATIC_ROOT)

DEFAULT_S3_PATH = '{}/media'.format(AWS_STORAGE_FOLDER_NAME)
DEFAULT_FILE_STORAGE = 's3_folder_storage.s3.DefaultStorage'
MEDIA_ROOT = '/{}/'.format(DEFAULT_S3_PATH)
MEDIA_URL = '{}{}'.format(AWS_S3_CUSTOM_DOMAIN, MEDIA_ROOT)

AWS_IS_GZIPPED = True
ADMIN_MEDIA_PREFIX = STATIC_URL + 'admin/'

AWS_HEADERS = {
    'Cache-Control': 'max-age=94608000',
}

GZIP_CONTENT_TYPES = (
   'text/css',
   'application/javascript',
   'application/x-javascript',
   'text/javascript',
   'application/vnd.ms-fontobject',
   'application/font-sfnt',
   'application/font-woff',
)


# Reset logging
LOGGING_CONFIG = None
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format': '[%(asctime)s] %(levelname)s [%(pathname)s:%(lineno)s] %(message)s',
            'datefmt': '%d/%b/%Y %H:%M:%S',
        },
        'simple': {'format': '%(levelname)s %(message)s'},
    },
    'handlers': {
        'proj_log_file': {
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'filename': str(LOGFILE_ROOT / 'project.log'),
            'formatter': 'verbose',
        },
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'simple',
        },
    },
    'loggers': {'project': {'handlers': ['proj_log_file'], 'level': 'DEBUG'}},
}

logging.config.dictConfig(LOGGING)
