from .base import *  # NOQA

import logging.config


# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True
TEMPLATES[0]['OPTIONS'].update({'debug': True})


# Less strict password authentication and validation
PASSWORD_HASHERS = [
    'django.contrib.auth.hashers.PBKDF2PasswordHasher',
    'django.contrib.auth.hashers.PBKDF2SHA1PasswordHasher',
    'django.contrib.auth.hashers.Argon2PasswordHasher',
    'django.contrib.auth.hashers.BCryptSHA256PasswordHasher',
    'django.contrib.auth.hashers.BCryptPasswordHasher',
]
AUTH_PASSWORD_VALIDATORS = []


ALLOWED_HOSTS = [
    '127.0.0.1',
]


# Django Debug Toolbar
INSTALLED_APPS += ['debug_toolbar']


# Additional middleware introduced by debug toolbar
MIDDLEWARE += ['debug_toolbar.middleware.DebugToolbarMiddleware']

# Show emails to console in DEBUG mode
EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'


# Allow internal IPs for debugging
INTERNAL_IPS = ['127.0.0.1', '0.0.0.1']


# Log everything to the logs directory at the top
LOGFILE_ROOT = BASE_DIR.parent / 'logs'


LOGGING_CONFIG = None
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format': '[%(asctime)s] %(levelname)s [%(pathname)s:%(lineno)s] %(message)s',
            'datefmt': '%d/%b/%Y %H:%M:%S',
        },
        'simple': {'format': '%(levelname)s %(message)s'},
    },
    'handlers': {
        'django_log_file': {
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'filename': str(LOGFILE_ROOT / 'django.log'),
            'formatter': 'verbose',
        },
        'proj_log_file': {
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'filename': str(LOGFILE_ROOT / 'project.log'),
            'formatter': 'verbose',
        },
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'simple',
        },
    },
    'loggers': {
        'django': {
            'handlers': ['django_log_file'],
            'propagate': True,
            'level': 'DEBUG',
        },
        'project': {'handlers': ['proj_log_file'], 'level': 'DEBUG'},
    },
}

logging.config.dictConfig(LOGGING)
