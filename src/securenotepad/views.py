from django.shortcuts import render


def manifest(request):
    return render(request, 'manifest.json', content_type='application/json')
